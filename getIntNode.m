function [node,score,nodeValue,nodes]= getIntNode(A,b,coef,x,int_number,step)
n=numel(x);
nodes=zeros(n,2*n);
score=func(coef,x);
node=x;
nodeValue=zeros(1,2*n);
for i=1:int_number
    nodes(:,2*i-1)=x;
    
    nodes(:,2*i)=x;
    nodes(i,2*i-1)=x(i)+1;
    nodes(i,2*i)=x(i)-1;
    if ~pointInSet(A,b,nodes(:,2*i-1))
        nodes(:,2*i-1)=x;
    end
    if ~pointInSet(A,b,nodes(:,2*i))
        nodes(:,2*i)=x;
    end
    nodeValue(:,2*i-1)=func(coef,nodes(:,2*i-1));
    nodeValue(:,2*i)=func(coef,nodes(:,2*i));
    
end
for i=int_number+1:n%TODO: Check this loop
     nodes(:,2*i-1)=x;
    nodes(:,2*i)=x;
    nodes(i,2*i-1)=x(i)+step;
    nodes(i,2*i)=x(i)-step;
    if ~pointInSet(A,b,nodes(:,2*i-1))
        nodes(:,2*i-1)=x;
    end
    if ~pointInSet(A,b,nodes(:,2*i))
        nodes(:,2*i)=x;

    end
    nodeValue(:,2*i-1)=func(coef,nodes(:,2*i-1));
    nodeValue(:,2*i)=func(coef,nodes(:,2*i));
    
end

[score,bestIndex]=max(nodeValue);
node=nodes(:,bestIndex);
end

