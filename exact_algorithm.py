import numpy as np
import math

c = np.array([[2, 3]], float)
A = np.array([[1, 1], [6, 3], [1, 2]], float)
b = np.array([100, 360, 120], float)


def initial_tableau(c, A, b):
    basic_variable = np.size(c, 1)
    F = np.multiply(c, -1)
    tableau = np.append(A, F, 0)
    slack = np.zeros((np.size(tableau, 0), np.size(tableau, 0) - 1), float)
    tableau = np.append(tableau, slack, 1)
    for i in range(np.size(tableau, 0) - 1):
        tableau[i][basic_variable + i] = 1
    tableau = np.append(tableau, np.append(b, 0).reshape(-1, 1), 1)
    return tableau


x = initial_tableau(c, A, b)


def get_pivot_column(tableau):
    pivot_column_value = 0
    pivot_column = 0
    for a in enumerate(tableau[-1][:-1]):
        if a[1] < 0 and a[1] < pivot_column_value:
            pivot_column = a[0]
            pivot_column_value = a[1]
    return pivot_column


def get_pivot_row(tableau, pivot_column):
    pivot_row_division = float('+inf')
    a = [0, 0]
    for a[0] in range(np.size(tableau, 0) - 1):
        a[1] = tableau[a[0]][pivot_column]
        if a[1] != 0 and tableau[a[0]][-1] / a[1] > 0 and tableau[a[0]][-1] / a[1] < pivot_row_division:
            pivot_row = a[0]
            pivot_row_division = tableau[a[0]][-1] / a[1]
    return pivot_row


def simplex_iteration(tableau, pivot_row, pivot_column):
    pivot_elem_value = tableau[pivot_row][pivot_column]
    for column in range(np.size(tableau, 1)):
        tableau[pivot_row][column] /= pivot_elem_value
    for row in range(np.size(tableau, 0)):
        multiplier = tableau[row][pivot_column]
        for column in range(np.size(tableau, 1)):
            if row != pivot_row:
                tableau[row][column] -= tableau[pivot_row][column] * multiplier
    return tableau


def round_tableau(tableau):
    for i in range(np.size(tableau, 0)):
        for j in range(np.size(tableau, 1)):
            if math.fabs(tableau[i][j] - round(tableau[i][j])) <= 1.e-5:
                tableau[i][j] = round(tableau[i][j])
            # tableau[i][j] = round(tableau[i][j], 3) # to make more readable
    return tableau


def can_improve_simplex(tableau):
    for i in range(np.size(tableau, 1) - 1):
        if tableau[-1][i] < 0:
            return True
    return False


def indexes_of_basis(tableau):
    indexes = []
    for column in range(np.size(tableau, 1) - 1):
        count_zeros = 0
        count_ones = 0
        for row in range(np.size(tableau, 0)):
            if math.fabs(tableau[row][column] - 0) <= 1.e-6:  # power of 10 should be less than round_tableau
                count_zeros += 1
            elif math.fabs(tableau[row][column] - 1) <= 1.e-6:
                count_ones += 1
        if count_ones == 1 and count_zeros == np.size(tableau, 0) - 1:
            indexes.append(column)
    return indexes


def get_x_values_n_rows(tableau):
    indexes = indexes_of_basis(tableau)
    basic_variables = np.size(c, 1)
    x_values = []
    x_rows = []
    for i in range(basic_variables):
        x_value = 0
        x_row = None
        if i in indexes:
            for j in range(np.size(tableau, 0)):
                if tableau[j][i] == 1:
                    x_value = tableau[j][-1]
                    x_row = j
            x_values.append(x_value)
            x_rows.append(x_row)
        else:
            x_values.append(x_value)
            x_rows.append(x_row)
    return x_values, x_rows


def function_value(x_values_n_rows):
    function = 0
    for i in range(np.size(c, 1)):
        function += c[0][i] * x_values_n_rows[0][i]
    return function


def simplex(tableau):
    while can_improve_simplex(tableau):
        pivot_column = get_pivot_column(tableau)
        pivot_row = get_pivot_row(tableau, pivot_column)
        tableau = simplex_iteration(tableau, pivot_row, pivot_column)
    tableau = round_tableau(tableau)
    return tableau


def find_row_for_cut(x_values_n_rows):
    cut_row = -1
    fractional_part = 0
    for count, i in enumerate(x_values_n_rows[0]):
        if i >= 0:
            if math.modf(i)[0] > fractional_part:
                fractional_part = math.modf(i)[0]
                cut_row = x_values_n_rows[1][count]
        else:
            if 1 + math.modf(i)[0] > fractional_part:
                fractional_part = 1 + math.modf(i)[0]
                cut_row = x_values_n_rows[1][count]
    return cut_row


def find_fractional_part(number):
    if number >= 0:
        return round(math.modf(number)[0], 5)
    else:
        return round(1 + math.modf(number)[0], 5)


def add_new_constraint(tableau, cut_row):
    tableau = np.insert(tableau, [-1], 0, axis=1)
    constraint = np.zeros((1, np.size(tableau, 1)), float)
    constraint[0][-1] = - find_fractional_part(tableau[cut_row][-1])
    constraint[0][-2] = 1
    for i in range(np.size(constraint, 1) - 2):
        if find_fractional_part(tableau[cut_row][i]) != 0:
            constraint[0][i] = - find_fractional_part(tableau[cut_row][i])
        else:
            constraint[0][i] = 0
    tableau = np.insert(tableau, [-1], constraint, axis=0)
    return tableau


def prepare_tableau(tableau):
    for i in range(np.size(tableau, 1)):
        tableau[-1][i] = - tableau[-1][i]
    return tableau


def find_pivot_column_for_cut(tableau):
    cut_column_division = float('+inf')
    for i in range(np.size(tableau, 1) - 2):
        if tableau[-2][i] != 0 and tableau[-1][i] / tableau[-2][i] < cut_column_division:
            cut_column_division = tableau[-1][i] / tableau[-2][i]
            cut_column = i
    return cut_column


def can_improve_gomori(tableau):
    for i in get_x_values_n_rows(tableau)[0]:
        if math.fabs(math.modf(i)[0]) >= 1.e-2:
            return True
    return False


def gomori(tableau):
    tableau = simplex(tableau)
    tableau = prepare_tableau(tableau)
    while can_improve_gomori(tableau):
        cut_row = find_row_for_cut(get_x_values_n_rows(tableau))
        tableau = add_new_constraint(tableau, cut_row)
        pivot_row_for_cut = np.size(tableau, 0) - 2
        pivot_column_for_cut = find_pivot_column_for_cut(tableau)
        tableau = simplex_iteration(tableau, pivot_row_for_cut, pivot_column_for_cut)
        tableau = round_tableau(tableau)
    x_values_n_rows = get_x_values_n_rows(tableau)
    function = function_value(x_values_n_rows)
    return x_values_n_rows[0], function


lol, kek = gomori(initial_tableau(c, A, b))
print(lol, '    ', kek)

'''
x = simplex(initial_tableau(c, A, b))
y = get_x_values_n_rows(x)
f = function_value(y)
#print(x)
#print(y)
#print(f)
test = add_new_constraint(x, find_row_for_cut(y))
print(test)
'''