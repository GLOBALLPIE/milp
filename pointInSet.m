function result = pointInSet(A,b,x)
for i=1:numel(b)
    if A(i,:)*x > b(i)
        result=false;
        return
    end
    
end
for i=1:numel(x)
    if x(i)<0
        result=false;
        return
    end
end
result=true;
end
