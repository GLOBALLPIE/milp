import numpy as np
from pulp import *


def hillClimbing(A, b, func_Coef, cPoint, integerNumber):
    """
    Finds the optimal point(maximum) for mixed integer linear problem
    :param integerNumber: number of first integer variables in the vector x
    :param initialPoint: the point from which we begin to go to the optimum
    """
    eps = 1e-7
    # currentPoint = np.copy(initialPoint)

    integerStep = np.array([1, -1])
    integerDirection = np.ones((np.shape(cPoint)[0], 1))  # TODO: check the size of vector

    step_Coef = 1.2
    continousDirection = np.ones((np.shape(cPoint)[0], 1))
    continousStep = np.array([-1 / step_Coef, -step_Coef, step_Coef, 1 / step_Coef])
    nPoint = np.copy(cPoint)
    loops = 0
    while True:
        loops += 1
        bestScore = function(func_Coef, cPoint)
        beforePoint = np.copy(cPoint)

        # loop for integer variables
        for i in range(0, integerNumber):
            bestStep = 0
            for step in integerStep:
                nPoint[i] = cPoint[i] + step * integerDirection[i]
                print("nPoint: ", nPoint.transpose())
                if pointInConstraints(A, b, nPoint) == False:
                    print("point is not in constrains")
                    continue
                else:
                    print("point is  in constrains")
                if function(func_Coef, nPoint) > bestScore:
                    bestScore = function(func_Coef, nPoint)
                    bestStep = step
                    print("it is better point")
            cPoint[i] = cPoint[i] + bestStep * integerDirection[i]
            nPoint = np.copy(cPoint)  # TODO: change it
            print("New point:", cPoint.transpose())
            print()
        # loop for continous variables
        i = integerNumber
        coordIsSmall = False
        while i != np.shape(cPoint)[0]:
            bestStep = 0
            for step in continousStep:
                nPoint[i] = cPoint[i] + step * continousDirection[i]
                print("continous nPoint: ", nPoint.transpose())
                if pointInConstraints(A, b, nPoint) == False:
                    print("point is not in constrains")
                    continue
                else:
                    print("point is in constrains")
                if function(func_Coef, nPoint) > bestScore:
                    bestScore = function(func_Coef, nPoint)
                    bestStep = step
                    print("it is better point")

            if bestStep != 0:
                cPoint[i] = cPoint[i] + bestStep * continousDirection[i]
                print("New point:", cPoint.transpose())
                continousDirection[i] *= bestStep
                print("New direction:", continousDirection.transpose())
                i += 1
            else:
                continousDirection[i] /= step_Coef
                coordIsSmall = abs(continousDirection[i]) < eps
                print("There are not good points. Reduce the set of coords")
                print("New direction:", continousDirection.transpose())
            print()
            nPoint = np.copy(cPoint)  # TODO: change it

            if coordIsSmall:
                print("direction is too small. Let's move to another coord")
                i += 1
                # continousDirection[i] = [1.0]
            print()
        print("beforePoint:", beforePoint.transpose())
        print("number of main loops", loops, '\n')
        # if abs(abs(function(func_Coef, beforePoint)) - abs(function(func_Coef, cPoint))) < eps:
        if np.linalg.norm(cPoint - beforePoint) < eps:
            print("Optimum value:", function(func_Coef, cPoint), '\n')
            return cPoint


def pointInConstraints(A, b, x):
    """
    Checks whether point satisfies all constraints or not
    """
    # for i in range(0, np.shape(x)[0]):
    #     if A[i].dot(x) > b[i, 0] or x[i, 0] < 0:
    #         return False
    # return True
    if False in (A.dot(x) <= b):
        return False
    else:
        return True


def function(func_coef, point):
    sum = 0
    for i in range(0, np.shape(point)[0]):
        sum += func_coef[0, i] * point[i, 0]
    return sum


# m = 2
# n = 2
# A = np.array([[2.0, 3.0], [2.0, - 3.0]])
# b = np.array([[6.0], [3.0]])
# initialPoint = np.array([[1], [0.0]])
# func_coef = np.array([[3.0, 1.0]])

m = 3
n = 2
A = np.array([[1.0, 1.0], [6.0, 3.0], [1, 2]])
b = np.array([[100.0], [360.0], [120.0]])
initialPoint = np.array([[10.0], [20.0]])
func_coef = np.array([[2.0, 3.0]])

# m = 2
# n = 3
# A = np.random.randn(m, n)
# b = np.random.randn(m, 1)
# func_Coef = np.random.randn(1, n)
# initialPoint = np.zeros((n, 1))
# print("Real optimum value:", function(func_Coef, np.array([[2.25], [0.5]])))

print(hillClimbing(A, b, func_coef, initialPoint, 2))

# PuLP library
variables = [LpVariable("x" + str(i), 0, cat=LpInteger) for i in range(0, n)]
problem = LpProblem("MILP", LpMaximize)

for i in range(0, n):
    problem += func_coef[0].dot(variables)

for i in range(0, m):
    problem += A[i].dot(variables) <= b[i, 0]

# print(problem)
status = problem.solve(PULP_CBC_CMD(msg=0))
print(LpStatus[status])
[print(value(variables[i])) for i in range(0, n)]

point = np.array([[34.0], [43.0]])

