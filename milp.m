close all
clear all
clc

m=3;
n=2;
A=[1 1; 6 3; 1 2];
b=[100;360;120];
x=[3; 2];
coef=[2 3];

m=5;
n=10;
A=rand(m,n)*10;
b=rand(m,1)*100;
x=zeros(n,1);
coef=rand(1,n)*10;

point=hill_climbing(A,b,x,coef,n-1,1e-7)
value=func(coef,point)
opt_point=intlinprog(-coef',[1 n-1],A,b,[],[],zeros(n,1),[],x);
display(opt_point)
opt_value=func(coef,opt_point)
% abs(opt_value-value)
% norm(point-optimal_point)
